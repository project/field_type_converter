# Field Type Converter

This a utility/developer-only module for converting fields from one type to another. E.g., for changing plain text (textarea) fields to filtered text (WYSIWYG), or short text fields to long text. Existing field data is preserved as long as the new field type schema has columns with the same names as the old schema (e.g., `value`). This module is based on the approach outlined in [this documentation](https://www.drupal.org/docs/drupal-apis/update-api/updating-entities-and-fields-in-drupal-8#s-updating-field-storage-config-items).

## Warnings

The script does not rename columns from the old field schema to the new (it just preserves columns that exist in both), so data will be lost if the new field type does not have corresponding columns for each column of the old field type. On very large databases or brittle infrastructure, the database queries can timeout, resulting in data loss. Be sure to test this in a staging environment first, and back-up the production database before executing the script. For a low-risk alternative, see the [Plain Text as Formatted](https://www.drupal.org/project/plain_text_as_formatted) module.

## Usage

Implement [hook_post_update_NAME()](https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Extension%21module.api.php/function/hook_post_update_NAME/10.0.x) and call `FieldTypeConverter::processBatch()`. E.g., to convert a plain-text node field named `field_foo` to long filtered text (i.e., WYSIWYG):

```php
function hook_post_update_NAME(&$sandbox) {
  $field_map['node'] = [
    'field_foo' => 'text_long',
  ];
  return \Drupal\field_type_converter\FieldTypeConverter\FieldTypeConverter::processBatch($sandbox, $field_map);
}
```
